***Settings***
Library  resource/charge.py
Library  JSONLibrary
Library  BuiltIn
Library  Collections

***Keywords***
get json
    [Arguments]  ${file_path}  ${type}
    ${data_as_string} =    Load JSON From File    ${file_path}
    ${key}=  Set Variable  ${data_as_string}
    ${key_} =  Get From Dictionary  ${key}  ${type}  
    [Return]  ${key_}
post charge
    [Arguments]  ${amount}  ${customer}  ${card}  ${key}  ${currency}
    ${data}=  input charge    ${amount}  ${customer}  ${card}  ${key}  ${currency}
    [Return]  ${data}
***Test Cases***
1.charge greater than or equal to 2000
    ${secret}=  get json  resource/json/key.json  secret
    ${data}=  post charge  2000  ${customer}  ${card}  ${secret}  ${currency}
    Should Be True  '${data}'=='successful'
2.charge less than 2000
    ${secret}=  get json  resource/json/key.json  secret
    ${data}=  post charge  1999  ${customer}  ${card}  ${secret}  ${currency}
    Should Be True  '${data}'=='error'
3.charge with wrong authen
    ${data}=  post charge  1999  ${customer}  ${card}  aaaaaaaaaaaaaaaa  ${currency}
    Should Be True  '${data}'=='error'
4.charge with non integer
    ${secret}=  get json  resource/json/key.json  secret
    ${data}=  post charge  twenty  ${customer}  ${card}  ${secret}  ${currency}
    Should Be True  '${data}'=='amount must be greater than or equal to ฿20 (2000 satangs)'
5.charge with wrong currency
    ${secret}=  get json  resource/json/key.json  secret
    ${data}=  post charge  2000  ${customer}  ${card}  ${secret}  abc
    Should Be True  '${data}'=='currency is currently not supported'

    



***Variables***
${card}  card_test_5h6dpkvh71cfgo09ut3
${customer}  cust_test_5h6dqarycc9icuil8ar
${currency}  thb