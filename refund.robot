***Settings***
Library  resource/refund.py
Library  JSONLibrary
Library  BuiltIn
Library  Collections

***Keywords***
get json
    [Arguments]  ${file_path}  ${type}
    ${data_as_string} =    Load JSON From File    ${file_path}
    ${key}=  Set Variable  ${data_as_string}
    ${key_} =  Get From Dictionary  ${key}  ${type}  
    [Return]  ${key_}
test refunds
    [Arguments]   ${secret_key}  ${charge_id}  ${amount}    
    ${object}=  test refund   ${secret_key}  ${charge_id}  ${amount}  
    [Return]    ${object}
    
***Test Cases***
1.refund success
    ${secret_key}=  get json  resource/json/key.json  secret
    ${object}=  test refunds  ${secret_key}  ${id_can_refund}  ${amount}    
    Should Be True  '${object}'=='refund'
2.refund more than charge
    ${secret_key}=  get json  resource/json/key.json  secret
    ${object}=  test refunds  ${secret_key}  ${id_can_refund}  200000000
    Should Be True  '${object}'=='amount cannot be higher than the remaining charge amount'
3.refund with LIMIT
    ${secret_key}=  get json  resource/json/key.json  secret
    :FOR    ${i}    IN RANGE    15
    \    ${object}=  test refunds  ${secret_key}  ${id_cannot_refund}  ${amount}
    \    Exit For Loop If    '${object}' == 'charge exceeds refund limit (15 refunds/charge)'
    Should Be True  '${object}'=='charge exceeds refund limit (15 refunds/charge)'



    
    

***Variables****
${id_cannot_refund}    chrg_test_5h587ehdhfq6jjux4i2
${id_can_refund}    chrg_test_5h6e7goad58jw7bg8a0
${amount}  2000

