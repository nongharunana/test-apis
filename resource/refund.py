import requests
from robot.api import logger

class refund():
  def test_refund(self,secret_key,charge_id,amount):
    data = {
      "amount":amount
    }
    url='https://api.omise.co/charges/'+charge_id+'/refunds'
    req_bfp = requests.get(url,auth=(secret_key, ''))
    logger.info(len(req_bfp.json()['data']))
    size_of_refund = len(req_bfp.json()['data'])
    if size_of_refund < 15:
      get_refund = requests.post(url,auth=(secret_key, ''),data=data)
      if get_refund.json()['object']=="refund":
        refund_id = get_refund.json()['id']
        resp = requests.get(f"https://api.omise.co/charges/{charge_id}/refunds/{refund_id}",auth=(secret_key, ''))
        if resp.status_code != 200:
          # This means something went wrong.
          logger.info(f'Error code status : {resp.status_code}')
        logger.info(resp.json())
        return resp.json()['object']
      else:
        resp=get_refund
        logger.info(resp.json()['object'])
        logger.info(resp.json()['message'])
        return resp.json()['message']
    else:
      get_refund = requests.post(url,auth=(secret_key, ''),data=data)
      return get_refund.json()['message']




    




