import requests
from robot.api import logger
class charge():

    def input_charge(self,amount,customer,card,key,currency):

        data={
            "amount":amount,
            "currency":currency,
            "customer":customer,
            "card":card
        }
        url = 'https://api.omise.co/charges'
        post_charge = requests.post(url,auth=(key, ''),data=data)
        if post_charge.json()['object']=='charge':
            charge_id = post_charge.json()['id']
            resp = requests.get(f"{url}/{charge_id}",auth=(key, ''))
            if resp.status_code != 200:
                # This means something went wrong.
                logger.info(f'Error code status : {resp.status_code}')
                # logger.info(f'Error code status : {resp.status_code}')

            return resp.json()['status']
        else:
            logger.info(post_charge.json()['object'])
            logger.info(post_charge.json()['message'])
            return post_charge.json()['message']  

